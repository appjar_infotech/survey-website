<?php include 'survey-header.php'; ?>
    <!-- End header -->

    <div class="section layout_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="full center">
                        <div class="heading_main text_align_center">
                            <h2><span class="theme_color">DOWNLOAD</span> APPLICATION FORM</h2>
                            <p class="large">Validate below captcha to download the form</p>
                            <div class="row justify-content-center">
                                <div class="col-12 col-md-10 col-lg-8">
                                    <form class="card card-sm" method="POST" action="application-status.php">
                                        <div class="card-body row no-gutters align-items-center">
                                            <div class="col-auto">
                                            </div>
                                            <!--end of col-->
                                            <div class="col">
                                                Google reCaptcha will<br> come here to avoid BOTs
                                            </div>
                                            <!--end of col-->
                                            <div class="col-auto">
                                                <a href="https://dashboard.delhistreethawker.com/print-survey-form" class="btn btn-lg btn-uri" type="submit">Download</a>
                                            </div>
                                            <!--end of col-->
                                        </div>
                                    </form>
                                </div>
                                <!--end of col-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php include 'survey-footer.php'; ?>
