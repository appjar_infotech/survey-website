		<!--================================FOOTER===========================================-->
		<div class="footer-clean">
        <footer id="footer">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-sm-4 col-md-3 item">
                        <ul>
                            <li><a style="color: white;" href="disclaimer">Disclaimer</a></li>
                            <li><a style="color: white;" href="privacy-policy">Privacy Policy</a></li>
                            <li><a style="color: white;" href="terms-and-conditions">Terms & Condition</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4 col-md-3 item">
                        <ul>
                           	<li><a  style="color: white;" href="gallery">Photo Gallery</a></li>
                            <li><a  style="color: white;" target="_blank" href="sdmc_1200.pdf">SDMC Map</a></li>
                            <li><a  style="color: white;" target="_blank" href="edmc.pdf">EDMC Map</a></li>
                        </ul>
                    </div>

                    <div class="col-sm-4 col-md-3 item">
                        <ul>
							<li><a style="color: white;" target="_blank" href="The Street Vendors  Act 2014.pdf">Street vendor Act 2014</a></li>
							<li><a style="color: white;" target="_blank" href="delhi_scheme_2019.pdf">Delhi Scheme 2019</a></li>
                        </ul>
                    </div>

            		<div class="col-lg-3 item social"><a href="#"><i class="icon ion-social-facebook"></i></a><a href="#"><i class="icon ion-social-twitter"></i></a><a href="#"><i class="icon ion-social-snapchat"></i></a><a href="#"><i class="icon ion-social-instagram"></i></a>
                        <p class="copyright"  style="color: white;">Office Location<br>C-124, Lagpat Nagar , New Delhi.</p>
                    </div>

                </div>
            </div>
        </footer>
    </div>
		<div class="footer-bottom" style="background-color: #15285c;">
			<!--footer bottom copyrights-->
			<div class="copyright">
			<p style="color: white;">©2020 Delhi Street Hawker All rights reserved</p>
			</div>
		</div>
		
        <!--================================JQuery===========================================-->
        
		<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
		<script src="js/jquery.js"></script><!-- jquery 1.11.2 -->
		<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
		
		<!--================================BOOTSTRAP===========================================-->
        <script src="js/bootstrap.min.js"></script>
		
		<!--================================PROGRESS BAR===========================================-->

		<!--================================RATINGS===========================================-->
		
		<script src="js/jquery.raty-fa.js"></script>
		<script src="js/rate.js"></script>
		
		<!--================================PROGRESS BAR===========================================-->
		
		<script src="js/pbar.js"></script>
		
		<!--================================GALLARY ===========================================-->
		
		<script src="js/jquery.isotope.min.js"></script><!-- isotop -->
        <script src="js/script.js"></script>
		
		<!--================================COLORBOX==========================================-->
		
		<script src="assets/colorbox/jquery.colorbox.js"></script>
		<script src="assets/colorbox/colorbox-triger.js"></script>
		
		
		<!--================================static header height===========================================-->
		<script type="text/javascript" src="js/height.js"></script>
		
		<!--================================OWL CARESOUL=============================================-->
		
		<script src="js/owl.carousel.js"></script>
        <script src="js/triger.js" type="text/javascript"></script>
		<!--================================form integration=============================================-->
		<script src="mail/jquery.ajaxchimp.min.js"></script>
		<script src="mail/mailchimp.js"></script>
		<script src="mail/form-triger.js"></script>
		<!--================================FunFacts Counter===========================================-->
		<script src="js/jquery.countTo.js"></script>
		<!--================================video===========================================-->
		<script src="assets/html5lightbox/html5lightbox.js"></script>
		<!--================================NAVIGATION===========================================-->
		
		<script type="text/javascript" src="js/navigation.js"></script>
		<script type="text/javascript" src="js/onepagescroll.js"></script>
		<!--================================waypoint===========================================-->
		
		<script type="text/javascript" src="js/jquery.waypoints.min.js"></script><!-- Countdown JS FILE -->
		
		<!--================================custom script===========================================-->
		<script type="text/javascript" src="js/custom.js"></script>
		<script src="js/jquery.vide.js"></script>
		<script type="text/javascript" src="assets/leaflet/leaflet.js"></script>
		<script type="text/javascript" src="assets/slickscroll/slick/slick.min.js"></script>
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>