<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="user-scalable = yes" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
        
	<title>Delhi Hawkers Survey</title>
	
    <meta name="discription" content="Delhi hawkers survey" >
	<meta name="keyword" content="Delhi hawkers survey">
    <!--================================BOOTSTRAP STYLE SHEETS================================-->
        
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	
    <!--================================ Main STYLE SHEETs====================================-->
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
	
	<!--================================ colors STYLE SHEETs====================================-->
	<link class="alt" href="css/colors/green.css" media="screen" rel="stylesheet"  title="color1" type="text/css">
	
	<!--================================COLORBOX==========================================-->

	<link rel="stylesheet" href="assets/colorbox/colorbox.css" />

	<!--================================FONTAWESOME==========================================-->
		
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        
	<!--================================GOOGLE FONTS=========================================-->
		
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,400,300,300italic,400italic,500,500italic,700,900' rel='stylesheet' type='text/css'>   
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,400italic' rel='stylesheet' type='text/css'>
		
</head>

<body>

    <!--===========================================MAIN NAV======================================-->
	<div id="page">
		<section id="nav">
			<header id="header" class="navbar-static-top">
				<div class="main-header">
					<div class="mobile-menu-wrap"><a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle"></a></div>
					<div class="container">
						<h1 class="logo navbar-brand">
							<a href="./">
								<img src="images/delhi_gov_logo.png">
							</a>
						</h1>

						<nav id="main-menu" role="navigation">
							<ul class="menu">
								<li class="menu-item">
									<a class="scroll" href="./">Home</a>
								</li>
    						    <li class="menu-item-has-children">
    								<a class="scroll" href="#page">Guidelines Act & Rule</a>
    								<ul style="left:0;">
    									<li><a class="scroll" target="_blank" href="The Street Vendors  Act 2014.pdf">Street vendor Act 2014</a></li>
    									<li><a class="scroll" target="_blank" href="delhi_scheme_2019.pdf">Delhi Scheme 2019</a></li>
    								</ul>
    							</li>
								<li class="menu-item">
									<a class="scroll" href="./#gallary">Gallery</a>
								</li>
								<li class="menu-item">
									<a class="scroll" href="./#faq">FAQ's</a>
								</li>
								<li class="menu-item">
									<a class="scroll" href="./#download-app">Download App</a>
								</li>
								<li class="menu-item">
									<a class="scroll" href="#">Contact Us</a>
								</li>
 								<li class="menu-item">
									<a class="scroll" href="https://dashboard.delhistreethawker.com">Login</a>
								</li>
								<li class="menu-item">
									<a class="scroll" href="hi/">हिन्दी</a>
								</li>
							</ul>
						</nav>
					</div>
					
					<nav id="mobile-menu-01" class="mobile-menu collapse">
						<ul id="mobile-primary-menu" class="menu">
							<li class="menu-item">
								<a class="scroll" href="./">Home</a>
							</li>
						    <li class="menu-item-has-children">
								<a class="scroll" href="#page">Guidelines Act & Rule</a>
								<ul style="left:0;">
									<li><a class="scroll" target="_blank" href="The Street Vendors  Act 2014.pdf">Street vendor Act 2014</a></li>
									<li><a class="scroll" target="_blank" href="delhi_scheme_2019.pdf">Delhi Scheme 2019</a></li>
								</ul>
							</li>
							<li class="menu-item">
								<a class="scroll" href="./#gallary">Gallery</a>
							</li>
							<li class="menu-item">
								<a class="scroll" href="./#faq">FAQ's</a>
							</li>
							<li class="menu-item">
								<a class="scroll" href="./#download-app">Download App</a>
							</li>

							<li class="menu-item">
								<a class="scroll" href="#">Contact Us</a>
							</li>
							<li class="menu-item">
								<a class="scroll" href="https://dashboard.delhistreethawker.com">Login</a>
							</li>
							<li class="menu-item">
								<a class="scroll" href="hi/">हिन्दी</a>
							</li>
						</ul>
					</nav>
				</div>
			</header>
		</section>
		<!--===========================================Static Header================================-->

		<section style="margin-top: 100px;" id="about-us-intro" class="os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0s">    
			<div class="container"> 
				<div class="row about-intro-main clearfix">
					<div class="col-xs-12">
						<div class="">
							<div class="section-title">
								<h4 style="text-align: center;">Application Status</h4>
								<div class="heading-bottom">
									<div class="line"></div>
									<div class="box-small"></div>
									<div class="big"></div>
									<div class="box-small"></div>
									<div class="line"></div>
								</div>
							</div>
<?php
	
	include_once("model/model.php");
	$model=new Model();
	$valid_request='<br><br><div class="section layout_padding">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="full center">
                                <div class="heading_main text_align_center" align="center">

                                    <u class="list-group">
                                        <li class="list-group-item"> URI ID : <%uri_id%> </li>
                                        <li class="list-group-item"> Vendor Name : <%vendor_name%> </li>
                                        <li class="list-group-item"> Corporation : <%corporation%> </li>
                                        <li class="list-group-item"> Zone : <%zone%> </li>
                                        <li class="list-group-item"> Ward : <%ward%> </li>
                                        <li class="list-group-item"> Area : <%area%> </li>
                                        <li class="list-group-item"> Status : <span style=""><%status%></span> </li>
                                    </u>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';

$invalid_request='<br><br><div class="section layout_padding">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="full center">
                                    <div class="heading_main" align="center">
                                        <u class="list-group" style="text-decoration: none;">
                                            <h3><li style="color:red;" class="list-group-item"><b><%res_msg%></b> : <%URI-ID%></h3></li>
                                        </u>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>';
	if(isset($_POST['uri_id']) and trim($_POST['uri_id'])!=""){
	    $result=$model->get_application_status($_POST['uri_id']);
	    if(isset($result)){
	        $valid_request=str_replace("<%uri_id%>", $result->getUriId(), $valid_request);
	        $valid_request=str_replace("<%vendor_name%>", $result->getVendorName(), $valid_request);
	        $valid_request=str_replace("<%corporation%>", $result->getCorporation(), $valid_request);
	        $valid_request=str_replace("<%zone%>", $result->getZone(), $valid_request);
	        $valid_request=str_replace("<%ward%>", $result->getWard(), $valid_request);
	        $valid_request=str_replace("<%area%>", $result->getArea(), $valid_request);
	        $valid_request=str_replace("<%status%>", $result->getStatus(), $valid_request);
	        echo $valid_request;
	    }else{
	        $invalid_request=str_replace("<%res_msg%>", "Invalid URI / Adhar Number", $invalid_request);
	        echo str_replace("<%URI-ID%>", $_POST['uri_id'], $invalid_request);
	    }
	}
	else{
	    $invalid_request=str_replace("<%res_msg%>", "Invalid URI / Adhar Number", $invalid_request);
	    echo str_replace("<%URI-ID%>", "", $invalid_request);
	}

?>
						</div>
					</div>
				</div>
					
			</div><!--about container closed closed -->
		</section>

		<!--================================FOOTER===========================================-->
<?php include('footer.php'); ?>
	</div>
    </body>
</html>
