<?php
	include_once("./model/mailerutility.php");
	$mailer = new mailerutility();

$errorMSG = "";
$code="";
$name="";
$msg="";
$phone="";
$email="";

/* NAME */
if (empty($_POST["uname"])) {
    $errorMSG = "Name is required";
    $code="1";
} else {
    $name = $_POST["uname"];
}


/* PHONE */
if (empty($_POST["uphone"])) {
    $errorMSG .= "Phone is required";
    $code="2";
} else if(strlen(preg_replace('/[^0-9]/', '', $_POST['uphone'])) === 10) {
    $phone = $_POST["uphone"];
}else {
    $code="2";
    $errorMSG .= "Mobile number should be 10 digit";
}


/* MSG SUBJECT */
if (empty($_POST["umsg"])) {
    $errorMSG .= "Message is required";
    $code="3";
} else {
    $msg = $_POST["umsg"];
}

/* EMAIL */
if (!empty($_POST["uemail"])) {
    $email = $_POST["uemail"];
}

$email_Content="Name : $name <br> Phone : $phone <br> Email : $email <br> Message : $msg";

if(empty($errorMSG)){
   $result = $mailer->sendMail('ameyprabhu74@gmail.com', $email_Content, 'Delhi Survey Enquiry');
    echo json_encode(['code'=>'200', 'msg'=>$result]);	
	exit;
}


echo json_encode(['code'=>$code, 'msg'=>$errorMSG]);

?>