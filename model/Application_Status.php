<?php

	class Application_Status{
		private $uri_id;
		private $vendor_name;
		private $corporation;
		private $zone;
		private $ward;
		private $area;
		private $status;

		public function setUriId($uri_id){
	        $this->uri_id = $uri_id;
	    }

	    public function setVendorName($vendor_name){
	        $this->vendor_name = $vendor_name;
	    }

	    public function setCorporation($corporation){
	        $this->corporation = $corporation;
	    }

	    public function setZone($zone){
	        $this->zone = $zone;
	    }

	    public function setWard($ward){
	        $this->ward = $ward;
	    }

	    public function setArea($area){
	        $this->area = $area;
	    }

	    public function setStatus($status){
	        $this->status = $status;
	    }
	    
	    public function getUriId(){
	        return $this->uri_id;
	    }

	   	public function getVendorName(){
	        return $this->vendor_name;
	    }

	    public function getCorporation(){
	        return $this->corporation;
	    }

	   	public function getZone(){
	        return $this->zone;
	    }

	   	public function getWard(){
	        return $this->ward;
	    }

	    public function getArea(){
	        return $this->area;
	    }

	   	public function getStatus(){
	        return $this->status;
	    }
	}

?>