<?php
include_once("Application_Status.php");
class Model {

    public $app_stat;
    protected static $bearer_token;
    protected static $url_head; 

    public function __construct()
  {
    self::$bearer_token='ce0b1330-da79-4cef-a16e-b3445f79417a';
    self::$url_head='https://dashboard.delhistreethawker.com';
  }

    public function get_application_status($uri_id)
    {
        if(isset($_POST['uri_id']) and trim($_POST['uri_id'])!=""){

            $uri_id=$_POST['uri_id'];
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => self::$url_head."/get-survey-details/$uri_id",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer ".self::$bearer_token,
                "Cookie: XSRF-TOKEN=eyJpdiI6IlZ4R0MwSjRKTUFXWEFYc0p0MERkaHc9PSIsInZhbHVlIjoiMU1nNEw1MG14Q3BwWkdsT0ZYWThONk9uYm5kejZrRjJGNTRRekFQamwrQXFucnZ6dzNUYitHSzErUmhYenY2YlNCNjdRYkNkdHBBMkZLVkUvU1R2Vi96VmdEMWxSSkJJcHpZQm9iclYwNHRXQXcxOThmQWlNZHBnbzNySDJuTFUiLCJtYWMiOiI0MDE2Yzg4YjkwOWIzYmVhMGViYjJlNGU4MzQ5ZTMwZjc1NmNiNzNjYmI0ZDk5OGNkNzkyMjBlOTZmNmEzZTE0In0%3D; surveyapp_session=eyJpdiI6IjZncFZnbnRWV1NuRXd0VUtTK2VkQUE9PSIsInZhbHVlIjoiK043V3NGdGN1MjI0ZjIxdWY3cm85UFF3aTVmeThUTWVjalVHcHBvU2F3Z0xNTXVyQ0Q1ZDFEVDU2SXNTenJxMlJtYnN4SmFuR3p4c1pTdEJJeXNCLzNUcXY3VmhtcjZlNEIyb29yNEdNejZ3cEtyQzBOa2NmQXdUQlZ2eU15WHgiLCJtYWMiOiJlN2VlZDA0ODA3NmFiMDdlZTdhMzdiZmU1YTE4N2FiMTBhNDNlOTYxM2UzM2QxNjVkMzdjYTg4NmZkNjhhNWJmIn0%3D"
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);

            $json = json_decode($response, true);

            return $this->check_response($json);
        }
    }

    private function check_response($response){
      try{
        if(isset($response)){
          if($response['status']==1){
            $this->app_stat = new Application_Status();
            $this->app_stat->setUriId($response['response']['uri_number']);
            $this->app_stat->setVendorName($response['response']['name_of_the_street_vendor']);
            $this->app_stat->setCorporation($response['response']['corporation']);
            $this->app_stat->setZone($response['response']['zone']);
            $this->app_stat->setWard($response['response']['ward']);
            $this->app_stat->setArea($response['response']['area']);
            switch ($response['response']['is_approved']) {
              case '0':
                $this->app_stat->setStatus("Pending");
                break;
              case '1':
                $this->app_stat->setStatus("Approved");
                break;
              case '2':
                $this->app_stat->setStatus("Rejected");
                break;

              default:
                $this->app_stat->setStatus("Pending");
                break;
            }
          }
        }
      }catch(Exception $e){}
        return $this->app_stat;
    }

    public function get_survey_stats(){

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => self::$url_head."/survey-data-count",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer ".self::$bearer_token,
            "Cookie: XSRF-TOKEN=eyJpdiI6IllBOFlvYlpKMGltaGdVZkVjeUxUVnc9PSIsInZhbHVlIjoidVVhdmRDbCswWHUxYi9qZVEzVlAvOTJLWjZBajJJQ0hoMHplcXJyS0JhOG9seFg1a1grTU5YVkRnVzJlQVpybnIyNEQrdlc5MTB3NWFpYnFvQmorcXhTUlNrRUlVNlBBK1VTUFU0cEo5eVFpWVhNWTc1ZTRIZ2pJUytZYzdxVnMiLCJtYWMiOiI5NzAyOGM2MmUwZmJjZjVlNTVkYmFjY2UzZGUzZTAwMzAyMjY5ODExYTY5YWVhNGMyMmI2M2RhNjI2YmZlNmFkIn0%3D; laravel_session=eyJpdiI6ImZ3K0xtR1UyVFlHV0lEOHlGdXhhVWc9PSIsInZhbHVlIjoiU3U3SHFtVjZHaFlBeHF4eGQ5akhrT3RJd3NFYXVydkI5Y2xZRUtEdS9rczBTQUpxQ3BaVU1ySHVaaUpDaisyYnVKV2dpeDVKYjhWSitlNjFqOGVlbC9NeVJURFZRN2cvV0I4WDJueFFXQ0xtRWZhY3ViUk81N3h2eGxIVnNIcisiLCJtYWMiOiI1MzkxYzk4YTQzODUyNzkzOTY3ZTMwMmU1ODQzYTAyMDIwNDY0Y2JlYTQ0ZDI3ZmE4NzYwZWZlNDUyMDQ3YzczIn0%3D"
          ),
        ));
            
            $result=array("total"=>0, "pending"=>0, "completed"=>0, "approved"=>0);
            $response = curl_exec($curl);

            curl_close($curl);

            if(isset($response)){
              $json = json_decode($response, true);

              if($json['status']==1){
                  $result["total"]=$json["response"]["total_survey"];
                  $result["pending"]=$json["response"]["pending_survey"];
                  $result["completed"]=$json["response"]["completed_survey"];
                  $result["approved"]=$json["response"]["approved_survey"];
              }
            }

            return $result;
    }

    public function sendMessage($uri_id, $zone_id, $name, $phone, $msg){
      
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => self::$url_head."/complaint-grievances?message=$msg&uri_no=$uri_id&phone_no=$phone&full_name=$name&zone_id=$zone_id",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer ".self::$bearer_token,
            "Cookie: XSRF-TOKEN=eyJpdiI6IjZXdjFHZGxVNStuSFNPL2NUODQybVE9PSIsInZhbHVlIjoiQ1I2THF5U0lvM0gwVlptMkJIL3FyYnN2VUwxK0N0UkVYVzBjSGdHRWREeVI5RUhmWFlqSUhjTzA4Tk1PQmVYeWVyRmVacFh3ek9CS1B2NnN4c0JQRVY2b0twczdUUkFEd1pBalBndXlZWjc4MXB3ZThLcGNtR1VKUHc3Y3cyVW8iLCJtYWMiOiIzYzE2MTBiMWM0Y2QxYmJkYzMzNzZkY2JiNzE1MDZlMWRkMjE1YjNkYmUyMDVkN2ZjYzQzNThlOWIwMTc2NTA4In0%3D; laravel_session=eyJpdiI6Im5wL3pJejcvWVRRV0pxZ0JWcDVmUkE9PSIsInZhbHVlIjoiUGF0bmllMVdLSG1lNng1VklKbjdQcDJYSDBacFI1anBscFJCcTFwTndXWTVtcllWWEw1TDRMdjZQU3AybDBwMElPRytGV0NEOFhMSWh2dFpTL1J1NjNidENYRHV3azhDWVRjZnllVDhoQ0hhN0ZKNDIwZUFHZXAvUnpia2hKN2wiLCJtYWMiOiI0NGQxYzZhNWJkYjVjZDM1OTA4OGFlZGY5ZjM1YThiMTlmZGVlZjQ0NzgxM2UyZDAxZTAxNmUzMjUyOWU3ZjFlIn0%3D"
          ),
        ));

      $response = curl_exec($curl);

      curl_close($curl);

      $result=array("code"=>"", "zone_id"=>"", "uri_no"=>"", "name"=>"", "msg"=>"", "phone"=>"", "ref_id"=>"");

      $json = json_decode($response, true);
      $result['code']='400';


            if($json['status']==1){
                $result['code']='200';
                $result['ref_id']=$json['response'];
            }else{
              $result['code']='400';
              if(isset($json['response']['zone_id'][0])){ $result["zone_id"]=$json['response']['zone_id'][0];}
              if(isset($json['response']['uri_no'][0])){ $result["uri_no"]=$json['response']['uri_no'][0];}
              if(isset($json['response']['full_name'][0])){ $result["name"]=$json['response']['full_name'][0];}
              if(isset($json['response']['message'][0])){ $result["msg"]=$json['response']['message'][0];}
              if(isset($json['response']['phone_no'][0])){ $result["phone"]=$json['response']['phone_no'][0];}
            }

            return $result;
    }

    public function get_complaint_msg($ref_id){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => self::$url_head."/complaint-grievances/$ref_id",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer ".self::$bearer_token,
            "Cookie: XSRF-TOKEN=eyJpdiI6IjY5M1JLclc2cmVuYytQQTJGQXBFMmc9PSIsInZhbHVlIjoiRjZRMHBhK2dPTnA5S1N6bFdIN3RFUG5TMG9ka2NaTGVyZXBTNDcxeXpFaEs5YWJxU0NCQmRvVUhlR3dIU3hqVGhxb09LNlYvR1BsYkR2T3NxWnVBdmN5NHVDdmd3WGZ2bXZjT3dMbU5xdE9QUHdMYzI2US8zMktNN0ZCVGw0U0MiLCJtYWMiOiI0NDkyNzkxYzdmMjBkMmFkZmM0ZGE0OTYzNzBiNmQ3NDljNjc3ZjQxNzZmOWE4MWFiNmM0YjNjZjM2ZDFhYWI2In0%3D; laravel_session=eyJpdiI6IjZzMDlPeURiVlpHWm9VeXFCVUpwMmc9PSIsInZhbHVlIjoiTzlSVjRYaG5JQlJkUkIxdXozZXRJMUNyLy95SmxBQ2FqWDlOVzk0blpBaDZ4UG11VE9uYmNWTmFaR1FxbmxmVTdHVEJzU0dDRmhxcWk0OEEyNlovaDBBK3RnR0E2aThsb3p6TGEvUjY5bzQwaUJNR0tocUJGS2dlTG80V1d4VlciLCJtYWMiOiI3NTg4ZDVhMjJjYjlmNDc5NDA4YzFlMDc2MmQwMTY2NTQ2ZmE0NjJjMmY1ZmNmYzA5YTA5NThmNzRiNDJlYTdhIn0%3D"
          ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        if(isset($response)){
          $json = json_decode($response, true);
          $result=array("ref_number"=>"", "corporation_name"=>"", "zone_name"=>"", "uri_no"=>"", "full_name"=>"", "message"=>"", "message_reply"=>"", "replied_by"=>"");
              if(isset($json['status']) and $json['status']==1){
                  $result["ref_number"]=$json["response"]["ref_number"];
                  $result["corporation_name"]=$json["response"]["corporation_name"];
                  $result["zone_name"]=$json["response"]["zone_name"];
                  $result["uri_no"]=$json["response"]["uri_no"];
                  $result["full_name"]=$json["response"]["full_name"];
                  $result["message"]=$json["response"]["message"];
                  $result["message_reply"]=$json["response"]["message_reply"];
                  $result["phone_no"]=$json["response"]["phone_no"];
                  $result["replied_by"]=$json["response"]["replied_by"];
              }
        }


            return $result;
    }

    public function get_map_plot(){
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://dashboard.delhistreethawker.com/survey-map-data/",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "Authorization: Bearer ce0b1330-da79-4cef-a16e-b3445f79417a"
        ),
      ));

      $response = curl_exec($curl);

      curl_close($curl);

      $json = json_decode($response, true);

      if($json['status'] == 1){
         return json_encode($json);
      }
     
    }

}

?>