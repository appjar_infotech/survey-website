<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="user-scalable = yes" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
        
	<title>Delhi Hawkers Survey</title>
	
    <meta name="discription" content="Delhi hawkers survey" >
	<meta name="keyword" content="Delhi hawkers survey">
    <!--================================BOOTSTRAP STYLE SHEETS================================-->
        
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
	
    <!--================================ Main STYLE SHEETs====================================-->
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="../css/responsive.css">
	
	<!--================================ colors STYLE SHEETs====================================-->
	<link class="alt" href="../css/colors/green.css" media="screen" rel="stylesheet"  title="color1" type="text/css">
	
	<!--================================COLORBOX==========================================-->

	<link rel="stylesheet" href="assets/colorbox/colorbox.css" />

	<!--================================FONTAWESOME==========================================-->
		
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        
	<!--================================GOOGLE FONTS=========================================-->
		
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,400,300,300italic,400italic,500,500italic,700,900' rel='stylesheet' type='text/css'>   
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,400italic' rel='stylesheet' type='text/css'>
		
</head>

<body>

    <!--===========================================MAIN NAV======================================-->
	<div id="page">
		<section id="nav">
			<header id="header" class="navbar-static-top">
				<div class="main-header">
					<div class="mobile-menu-wrap"><a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle"></a></div>
					<div class="container">
						<h1 class="logo navbar-brand">
							<a href="./">
								<img src="../images/delhi_gov_logo.png">
							</a>
						</h1>

						<nav id="main-menu" role="navigation">
							<ul class="menu">
								<li class="menu-item">
									<a class="scroll" href="./">Home</a>
								</li>
    						    <li class="menu-item-has-children">
    								<a class="scroll" href="#page">Guidelines Act & Rule</a>
    								<ul style="left:0;">
    									<li><a class="scroll" target="_blank" href="The Street Vendors  Act 2014.pdf">Street vendor Act 2014</a></li>
    									<li><a class="scroll" target="_blank" href="delhi_scheme_2019.pdf">Delhi Scheme 2019</a></li>
    								</ul>
    							</li>
								<li class="menu-item">
									<a class="scroll" href="./#gallary">Gallery</a>
								</li>
								<li class="menu-item">
									<a class="scroll" href="./#faq">FAQ's</a>
								</li>
								<li class="menu-item">
									<a class="scroll" href="./#download-app">Download App</a>
								</li>
								<li class="menu-item">
									<a class="scroll" href="#">Contact Us</a>
								</li>
 								<li class="menu-item">
									<a class="scroll" href="https://dashboard.delhistreethawker.com">Login</a>
								</li>
								<li class="menu-item">
									<a class="scroll" href="hi/">हिन्दी</a>
								</li>
							</ul>
						</nav>
					</div>
					
					<nav id="mobile-menu-01" class="mobile-menu collapse">
						<ul id="mobile-primary-menu" class="menu">
							<li class="menu-item">
								<a class="scroll" href="./">Home</a>
							</li>
						    <li class="menu-item-has-children">
								<a class="scroll" href="#page">Guidelines Act & Rule</a>
								<ul style="left:0;">
									<li><a class="scroll" target="_blank" href="The Street Vendors  Act 2014.pdf">Street vendor Act 2014</a></li>
									<li><a class="scroll" target="_blank" href="delhi_scheme_2019.pdf">Delhi Scheme 2019</a></li>
								</ul>
							</li>
							<li class="menu-item">
								<a class="scroll" href="./#gallary">Gallery</a>
							</li>
							<li class="menu-item">
								<a class="scroll" href="./#faq">FAQ's</a>
							</li>
							<li class="menu-item">
								<a class="scroll" href="./#download-app">Download App</a>
							</li>

							<li class="menu-item">
								<a class="scroll" href="#">Contact Us</a>
							</li>
							<li class="menu-item">
								<a class="scroll" href="https://dashboard.delhistreethawker.com">Login</a>
							</li>
							<li class="menu-item">
								<a class="scroll" href="hi/">हिन्दी</a>
							</li>
						</ul>
					</nav>
				</div>
			</header>
		</section>

		<section style="margin-top: 100px;" id="about-us-intro" class="os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0s">    
			<div class="container"> 
				<div class="row about-intro-main clearfix">
					<div class="col-xs-12">
						<div class="">
							<div class="section-title">
								<h4 style="text-align: center;">अस्वीकरण</h4>
								<div class="heading-bottom">
									<div class="line"></div>
									<div class="box-small"></div>
									<div class="big"></div>
									<div class="box-small"></div>
									<div class="line"></div>
								</div>
							</div>
								<p style="text-align: justify; font-size: 15px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
						</div>
					</div>
				</div>
					
			</div><!--about container closed closed -->
		</section>
	<?php include('footer.php'); ?>
