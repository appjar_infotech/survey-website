<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="user-scalable = yes" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
  <title>Delhi Hawkers Survey</title>
  
    <meta name="discription" content="Delhi hawkers survey" >
  <meta name="keyword" content="Delhi hawkers survey">
    <!--================================BOOTSTRAP STYLE SHEETS================================-->
        
  <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
  
    <!--================================ Main STYLE SHEETs====================================-->
  <link rel="stylesheet" type="text/css" href="../css/style.css">
  <link rel="stylesheet" type="text/css" href="../css/responsive.css">
  
  <!--================================ colors STYLE SHEETs====================================-->
  <link class="alt" href="../css/colors/green.css" media="screen" rel="stylesheet"  title="color1" type="text/css">
  
  <!--================================COLORBOX==========================================-->

  <link rel="stylesheet" href="assets/colorbox/colorbox.css" />

  <!--================================FONTAWESOME==========================================-->
    
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        
  <!--================================GOOGLE FONTS=========================================-->
    
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,400,300,300italic,400italic,500,500italic,700,900' rel='stylesheet' type='text/css'>   
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,400italic' rel='stylesheet' type='text/css'>
    
</head>

<body>

    <!--===========================================MAIN NAV======================================-->
  <div id="page">
		<section id="nav">
			<header id="header" class="navbar-static-top">
				<div class="main-header">
					<div class="mobile-menu-wrap"><a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle"></a></div>
					<div class="container">
						<h1 class="logo navbar-brand">
							<a href="./">
								<img src="../images/delhi_gov_logo.png" height="85px;">
							</a>
						</h1>

						<nav id="main-menu" role="navigation">
							<ul class="menu">
								<li class="menu-item active">
									<a class="scroll" href="./">मुख्य पृष्ठ</a>
								</li>
								<li class="menu-item-has-children">
    								<a class="scroll" href="#page">दिशानिर्देश अधिनियम और नियम</a>
    								<ul style="left:0;">
    									<li><a class="scroll" target="_blank" href="The Street Vendors  Act 2014.pdf">स्ट्रीट विक्रेता अधिनियम २०१४  </a></li>
    									<li><a class="scroll" target="_blank" href="delhi_scheme_2019.pdf">
दिल्ली हॉकर योजना २०१९  </a></li>
    								</ul>
								</li>
								<li class="menu-item">
									<a class="scroll" href="gallery">चित्र प्रदर्शनी</a>
								</li>
								<li class="menu-item">
									<a class="scroll" href="./#faq">सामान्य प्रश्न</a>
								</li>
								<li class="menu-item">
									<a class="scroll" href="./#download-app">
डाउनलोड 
एप्लिकेशन</a>
								</li>
								<li class="menu-item">
									<a class="scroll" href="#contact">संपर्क करें</a>
								</li>
 								<li class="menu-item">
									<a class="scroll" href="https://dashboard.delhistreethawker.com">
लॉग इन</a>
								</li>
								<li class="menu-item">
									<a class="scroll" href="../">English</a>
								</li>
							</ul>
						</nav>
					</div>
					
					<nav id="mobile-menu-01" class="mobile-menu collapse">
						<ul id="mobile-primary-menu" class="menu">
							<li class="menu-item active">
								<a class="scroll" href="./">मुख पृष्ठ</a>
							</li>
							<li class="menu-item-has-children">
								<a class="scroll" href="#page">दिशानिर्देश अधिनियम और नियम</a>
								<ul style="left:0;">
									<li><a class="scroll" target="_blank" href="The Street Vendors  Act 2014.pdf">स्ट्रीट विक्रेता अधिनियम २०१४  </a></li>
									<li><a class="scroll" target="_blank" href="delhi_scheme_2019.pdf">
दिल्ली हॉकर योजना २०१९  </a></li>
								</ul>
							</li>
							<li class="menu-item">
								<a class="scroll" href="gallery">चित्र प्रदर्शनी</a>
							</li>
							<li class="menu-item">
								<a class="scroll" href="./#faq">सामान्य प्रश्न</a>
							</li>
							<li class="menu-item">
								<a class="scroll" href="./#download-app">
डाउनलोड 
एप्लिकेशन</a>
							</li>

							<li class="menu-item">
								<a class="scroll" href="#contact">संपर्क करें</a>
							</li>
							<li class="menu-item">
								<a class="scroll" href="https://dashboard.delhistreethawker.com">
लॉग इन</a>
							</li>
							<li class="menu-item">
								<a class="scroll" href="../">English</a>
							</li>
						</ul>
					</nav>
				</div>
			</header>
		</section>
		<!--===========================================Static Header================================-->

		
		<!--===========================================COUNTER================================-->


		
		<!--================================ABOUT===========================================-->
<!-- 		<section id="about-us" class="os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0s">    
			<div class="container">  
				<div class="row about-block-main clearfix"><!--about us features block ->
					<div class="col-xs-12 col-sm-3 col-md-3"><!--about us feature 1st ->
						<div class="about-block clearfix">
							
							<div class='circle'>
								<i class="fa fa-heart"></i>
							</div>
								
							<div class="heading">
								<h6>DONATE</h6>
								<p>Lorem ipsum dolor sit amet consectetuer dipiscing elit sed diam nonummy nibh euismod tinci laoreet dolore magna volutpat.</p>
							</div>	
						</div>
					</div><!--about us feature 1st closed ->
						
					<div class="col-xs-12 col-sm-3 col-md-3"><!--about us feature 2nd ->
						<div class="about-block clearfix">
								
							<div class='circle'>
								<i class="fa fa-leaf"></i>
							</div>
								
							<div class="heading">
								<h6>FUNDRAISE</h6>
								<p>Lorem ipsum dolor sit amet consectetuer dipiscing elit sed diam nonummy nibh euismod tinci laoreet dolore magna volutpat.</p>
							</div>
						</div>
					</div><!--about us feature 2nd closed ->
						
					<div class="col-xs-12 col-sm-3 col-md-3"><!--about us feature 3rd ->
						<div class="about-block clearfix">
								
							<div class='circle'>
								<i class="fa fa-users"></i>
							</div>
								
							<div class="heading">
								<h6>PARTICIPATE</h6>
								<p>Lorem ipsum dolor sit amet consectetuer dipiscing elit sed diam nonummy nibh euismod tinci laoreet dolore magna volutpat.</p>
							</div>
						</div>
					</div><!--about us feature 3rd closed ->
					
					<div class="col-xs-12 col-sm-3 col-md-3"><!--about us feature 4th ->
						<div class="about-block clearfix">
								
							<div class='circle'>
								<i class="fa fa-users"></i>
							</div>
								
							<div class="heading">
								<h6>PARTICIPATE</h6>
								<p>Lorem ipsum dolor sit amet consectetuer dipiscing elit sed diam nonummy nibh euismod tinci laoreet dolore magna volutpat.</p>
							</div>
						</div>
					</div><!--about us feature 4th closed ->
					
				</div><!--about us features block closed ->
			</div><!--about container closed closed ->
		</section> -->
		<section style="margin-top: 100px;" id="about-us-intro" class="os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0s">    
			<div class="container"> 
				<div class="row about-intro-main clearfix">
					<div class="col-xs-12">
						<div class="">
							<div class="section-title">
								<h4 style="text-align: center;">आवेदन की स्थिति</h4>
								<div class="heading-bottom">
									<div class="line"></div>
									<div class="box-small"></div>
									<div class="big"></div>
									<div class="box-small"></div>
									<div class="line"></div>
								</div>
							</div>
<?php
	
	include_once("model/model.php");
	$model=new Model();
	$valid_request='<br><br><div class="section layout_padding">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="full center">
                                <div class="heading_main" align="center">

                                    <u class="list-group">
                                        <li class="list-group-item"> यू आर आई नंबर : <%uri_id%> </li>
                                        <li class="list-group-item"> विक्रेता का नाम : <%vendor_name%> </li>
                                        <li class="list-group-item"> निगम : <%corporation%> </li>
                                        <li class="list-group-item"> क्षेत्र : <%zone%> </li>
                                        <li class="list-group-item"> वार्ड : <%ward%> </li>
                                        <li class="list-group-item"> इलाका : <%area%> </li>
                                        <li class="list-group-item"> स्थिति : <span style=""><%status%></span> </li>
                                    </u>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';

$invalid_request='<br><br><div class="section layout_padding">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="full center">
                                    <div class="heading_main text_align_center">
                                        <u class="list-group" style="text-decoration: none;">
                                            <h3><li style="color:red;" class="list-group-item"><b><%res_msg%></b> : <%URI-ID%></h3></li>
                                        </u>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>';
	if(isset($_POST['uri_id']) and trim($_POST['uri_id'])!=""){
	    $result=$model->get_application_status($_POST['uri_id']);
	    if(isset($result)){
	        $valid_request=str_replace("<%uri_id%>", $result->getUriId(), $valid_request);
	        $valid_request=str_replace("<%vendor_name%>", $result->getVendorName(), $valid_request);
	        $valid_request=str_replace("<%corporation%>", $result->getCorporation(), $valid_request);
	        $valid_request=str_replace("<%zone%>", $result->getZone(), $valid_request);
	        $valid_request=str_replace("<%ward%>", $result->getWard(), $valid_request);
	        $valid_request=str_replace("<%area%>", $result->getArea(), $valid_request);
	        $valid_request=str_replace("<%status%>", $result->getStatus(), $valid_request);
	        echo $valid_request;
	    }else{
	        $invalid_request=str_replace("<%res_msg%>", "अमान्य यू आर आई नंबर", $invalid_request);
	        echo str_replace("<%URI-ID%>", $_POST['uri_id'], $invalid_request);
	    }
	}
	else{
	    $invalid_request=str_replace("<%res_msg%>", "अमान्य यू आर आई नंबर", $invalid_request);
	    echo str_replace("<%URI-ID%>", "", $invalid_request);
	}

?>
						</div>
					</div>
				</div>
					
			</div><!--about container closed closed -->
		</section>

		<!--================================FOOTER===========================================-->
<?php include('footer.php'); ?>
	</div>


    </body>
</html>
