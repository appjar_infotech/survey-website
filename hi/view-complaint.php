<?php
  include_once("model/model.php");
  $model=new Model();
  if(isset($_POST['ref_id'])){
    $result = $model->get_complaint_msg($_POST['ref_id']);
    if(!isset($result["ref_number"]) or $result["ref_number"]==""){
        $result="400";
    }
  }else{
      $result="400";
  }
  
?>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="user-scalable = yes" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
  <title>Delhi Hawkers Survey</title>
  
    <meta name="discription" content="Delhi hawkers survey" >
  <meta name="keyword" content="Delhi hawkers survey">
    <!--================================BOOTSTRAP STYLE SHEETS================================-->
        
  <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
  
    <!--================================ Main STYLE SHEETs====================================-->
  <link rel="stylesheet" type="text/css" href="../css/style.css">
  <link rel="stylesheet" type="text/css" href="../css/responsive.css">
  
  <!--================================ colors STYLE SHEETs====================================-->
  <link class="alt" href="../css/colors/green.css" media="screen" rel="stylesheet"  title="color1" type="text/css">
  
  <!--================================COLORBOX==========================================-->

  <link rel="stylesheet" href="assets/colorbox/colorbox.css" />

  <!--================================FONTAWESOME==========================================-->
    
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        
  <!--================================GOOGLE FONTS=========================================-->
    
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,400,300,300italic,400italic,500,500italic,700,900' rel='stylesheet' type='text/css'>   
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,400italic' rel='stylesheet' type='text/css'>
    
</head>

<body>

    <!--===========================================MAIN NAV======================================-->
  <div id="page">
    <section id="nav">
      <header id="header" class="navbar-static-top">
        <div class="main-header">
          <div class="mobile-menu-wrap"><a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle"></a></div>
          <div class="container">
            <h1 class="logo navbar-brand">
              <a href="./">
                <img src="../images/delhi_gov_logo.png" height="85px;">
              </a>
            </h1>

            <nav id="main-menu" role="navigation">
              <ul class="menu">
                <li class="menu-item active">
                  <a class="scroll" href="./">मुख्य पृष्ठ</a>
                </li>
                <li class="menu-item-has-children">
                    <a class="scroll" href="#page">दिशानिर्देश अधिनियम और नियम</a>
                    <ul style="left:0;">
                      <li><a class="scroll" target="_blank" href="The Street Vendors  Act 2014.pdf">स्ट्रीट विक्रेता अधिनियम २०१४  </a></li>
                      <li><a class="scroll" target="_blank" href="delhi_scheme_2019.pdf">
दिल्ली हॉकर योजना २०१९  </a></li>
                    </ul>
                </li>
                <li class="menu-item">
                  <a class="scroll" href="gallery">चित्र प्रदर्शनी</a>
                </li>
                <li class="menu-item">
                  <a class="scroll" href="./#faq">सामान्य प्रश्न</a>
                </li>
                <li class="menu-item">
                  <a class="scroll" href="./#download-app">
डाउनलोड 
एप्लिकेशन</a>
                </li>
                <li class="menu-item">
                  <a class="scroll" href="#contact">संपर्क करें</a>
                </li>
                <li class="menu-item">
                  <a class="scroll" href="https://dashboard.delhistreethawker.com">
लॉग इन</a>
                </li>
                <li class="menu-item">
                  <a class="scroll" href="../">English</a>
                </li>
              </ul>
            </nav>
          </div>
          
          <nav id="mobile-menu-01" class="mobile-menu collapse">
            <ul id="mobile-primary-menu" class="menu">
              <li class="menu-item active">
                <a class="scroll" href="./">मुख पृष्ठ</a>
              </li>
                <li class="menu-item-has-children">
                    <a class="scroll" href="#page">दिशानिर्देश अधिनियम और नियम</a>
                    <ul style="left:0;">
                      <li><a class="scroll" target="_blank" href="The Street Vendors  Act 2014.pdf">स्ट्रीट विक्रेता अधिनियम २०१४  </a></li>
                      <li><a class="scroll" target="_blank" href="delhi_scheme_2019.pdf">
दिल्ली हॉकर योजना २०१९  </a></li>
                    </ul>
                </li>
              <li class="menu-item">
                <a class="scroll" href="gallery">चित्र प्रदर्शनी</a>
              </li>
              <li class="menu-item">
                <a class="scroll" href="./#faq">सामान्य प्रश्न</a>
              </li>
              <li class="menu-item">
                <a class="scroll" href="./#download-app">
डाउनलोड 
एप्लिकेशन</a>
              </li>

              <li class="menu-item">
                <a class="scroll" href="#contact">संपर्क करें</a>
              </li>
              <li class="menu-item">
                <a class="scroll" href="https://dashboard.delhistreethawker.com">
लॉग इन</a>
              </li>
              <li class="menu-item">
                <a class="scroll" href="../">English</a>
              </li>
            </ul>
          </nav>
        </div>
      </header>
    </section>

    <section style="margin-top: 100px;" id="about-us-intro" class="os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0s">    
      <div class="container"> 
        <div class="row about-intro-main clearfix">
          <div class="col-xs-12">
            <div class="">
              <div class="section-title">
                <h4 style="text-align: center;"> 
जांच या शिकायत की स्थिति</h4>
                <div class="heading-bottom">
                  <div class="line"></div>
                  <div class="box-small"></div>
                  <div class="big"></div>
                  <div class="box-small"></div>
                  <div class="line"></div>
                </div>
              </div>
    <div class="row">
    <div class="col-md-12">
        <div class="blog-comment">
                <hr/>
        <ul class="comments">
        <li class="clearfix">
         <?php 
            if($result=="400"){  
              echo "<div align='center'><h5 style='color:red;'>अमान्य संदर्भ संख्या !</h5></div>";
            } 
            else{
                echo "<div class='post-comments'>
                <p class='meta'>  <a href='#'>".$result["full_name"]."</a> -  शिकायत कर्ता  <i class='pull-right'><a href='#'></a></i></p>
                <p>
                ".$result['message']."
                </p>
                </div>";

                if(isset($result['message_reply']) or $result['message_reply']!=null){
                  echo "<ul class='comments'>
                  <li class='clearfix'>
                  <div class='post-comments'>
                  <p class='meta'> <a href='#'>".$result["replied_by"]."</a> - सरकारी अधिकारी<i class='pull-right'><a href='#'></a></i></p>
                  <p>
                  ".$result['message_reply']."
                  </p>
                  </div>
                  </li>
                  </ul>";
                }else{
                  echo "<div align='center'><h6 style='color:red;'>प्रदर्शित करने के लिए कोई टिप्पणी नहीं !</h6></div>";
                }
            } 
          ?>
          </li>
        </ul>
      </div>
    </div>
  </div>
            </div>
          </div>
        </div>
          
      </div><!--about container closed closed -->
    </section>

    <!--================================FOOTER===========================================-->
    <!--================================FOOTER===========================================-->
<?php include('footer.php'); ?>
  </div>

    </body>
</html>

<style type="text/css">
 hr {
    margin-top: 20px;
    margin-bottom: 20px;
    border: 0;
    border-top: 1px solid #FFFFFF;
}
a {
    color: #82b440;
    text-decoration: none;
}
.blog-comment::before,
.blog-comment::after,
.blog-comment-form::before,
.blog-comment-form::after{
    content: "";
  display: table;
  clear: both;
}

.blog-comment{
    padding-left: 15%;
  padding-right: 15%;
}

.blog-comment ul{
  list-style-type: none;
  padding: 0;
}

.blog-comment img{
  opacity: 1;
  filter: Alpha(opacity=100);
  -webkit-border-radius: 4px;
     -moz-border-radius: 4px;
       -o-border-radius: 4px;
      border-radius: 4px;
}

.blog-comment img.avatar {
  position: relative;
  float: left;
  margin-left: 0;
  margin-top: 0;
  width: 65px;
  height: 65px;
}

.blog-comment .post-comments{
  border: 1px solid #eee;
    margin-bottom: 20px;
    margin-left: 85px;
  margin-right: 0px;
    padding: 10px 20px;
    position: relative;
    -webkit-border-radius: 4px;
       -moz-border-radius: 4px;
         -o-border-radius: 4px;
        border-radius: 4px;
  background: #fff;
  color: #6b6e80;
  position: relative;
}

.blog-comment .meta {
  font-size: 13px;
  color: #aaaaaa;
  padding-bottom: 8px;
  margin-bottom: 10px !important;
  border-bottom: 1px solid #eee;
}

.blog-comment ul.comments ul{
  list-style-type: none;
  padding: 0;
  margin-left: 85px;
}

.blog-comment-form{
  padding-left: 15%;
  padding-right: 15%;
  padding-top: 40px;
}

.blog-comment h3,
.blog-comment-form h3{
  margin-bottom: 40px;
  font-size: 26px;
  line-height: 30px;
  font-weight: 800;
}
                         
</style>