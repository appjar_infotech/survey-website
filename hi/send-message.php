<?php
	include_once("model/model.php");
	$model=new Model();

$errorMSG = "";
$code="";
$name="";
$zone_id="";
$msg="";
$phone="";
$uri_id="";

/* ZONE */
if (empty($_POST["zone_id"])) {
    $errorMSG = "zone is required";
    $code="0";
} else {
    $zone_id = $_POST["zone_id"];
}

/* NAME */
if (empty($_POST["name"])) {
    $errorMSG = "Full Name is required";
    $code="1";
} else {
    $name = $_POST["name"];
}


/* PHONE */
if (empty($_POST["phone"])) {
    $errorMSG .= "Phone is required";
    $code="2";
} else if(strlen(preg_replace('/[^0-9]/', '', $_POST['phone'])) === 10) {
    $phone = $_POST["phone"];
}else {
    $code="2";
    $errorMSG .= "मोबाइल नंबर 10 अंकों का होना चाहिए";
}


/* MSG SUBJECT */
if (empty($_POST["msg"])) {
    $errorMSG .= "Message is required";
    $code="3";
} else {
    $msg = $_POST["msg"];
}


/* MESSAGE */
if (empty($_POST["uri_id"])) {
    $errorMSG .= "URI number is required";
    $code="4";
} else {
    $uri_id = $_POST["uri_id"];
}


if(empty($errorMSG)){

	$msgy = "Name: ".$name.", msg: ".$msg.", phone: ".$phone.", uri_id:".$uri_id;
	$result=$model->sendMessage(urlencode($uri_id), urlencode($zone_id), urlencode($name), urlencode($phone), urlencode($msg));

	if($result['code']=='200'){

		echo json_encode(['code'=>$result['code'], 'msg'=>"हमें आपका संदेश मिल गया है, आपका संदर्भ नंबर है : ".$result['ref_id']]);

	}else if($result['code']=='400'){
		$response="";
		      if(isset($result['zone_id']) and trim($result['zone_id'])!=""){ $response.=$result['zone_id']."<br>"; }
              if(isset($result['uri_no']) and trim($result['uri_no'])!=""){ $response.="अवैध यू आर आई नंबर<br>"; }
              if(isset($result['name']) and trim($result['name'])!=""){ $response.=$result['name']."<br>"; }
              if(isset($result['msg']) and trim($result['msg'])!=""){ $response.=$result['msg']."<br>"; }
              if(isset($result['phone']) and trim($result['phone'])!=""){ $response.=$result['phone']."<br>"; }
		echo json_encode(['code'=>$result['code'], 'msg'=>$response]);

	}else{
		echo json_encode(['code'=>'0000', 'msg'=>'Pleaes try again !']);
	}
	
	exit;
}


echo json_encode(['code'=>$code, 'msg'=>$errorMSG]);
?>