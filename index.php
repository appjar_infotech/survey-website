<?php
	include_once("model/model.php");
	$model=new Model();
	$survey_stats = $model->get_survey_stats();
?>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="user-scalable = yes" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
        
	<title>Delhi Hawkers Survey</title>
	
    <meta name="discription" content="Delhi hawkers survey" >
	<meta name="keyword" content="Delhi hawkers survey">
    <!--================================BOOTSTRAP STYLE SHEETS================================-->
        
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	
    <!--================================ Main STYLE SHEETs====================================-->
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
	
	<!--================================ colors STYLE SHEETs====================================-->
	<link class="alt" href="css/colors/green.css" media="screen" rel="stylesheet"  title="color1" type="text/css">
	
	<!--================================COLORBOX==========================================-->

	<link rel="stylesheet" href="assets/colorbox/colorbox.css" />

	<!--================================FONTAWESOME==========================================-->
		
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">

    <link rel="stylesheet" type="text/css" href="assets/leaflet/leaflet.css">
        
	<!--================================GOOGLE FONTS=========================================-->
		
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,400,300,300italic,400italic,500,500italic,700,900' rel='stylesheet' type='text/css'>   
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,400italic' rel='stylesheet' type='text/css'>
		
</head>

<body>

    <!--===========================================MAIN NAV======================================-->
	<div id="page">
		<section id="nav">
			<header id="header" class="navbar-static-top">
				<div class="main-header">
					<div class="mobile-menu-wrap"><a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle"></a></div>
					<div class="container">
						<h1 class="logo navbar-brand">
							<a href="./">
								<img src="images/delhi_gov_logo.png">
							</a>
						</h1>

						<nav id="main-menu" role="navigation">
							<ul class="menu">
								<li class="menu-item active">
									<a class="scroll" style="font-size:15px;" href="#page">Home</a>
								</li>
						        <li class="menu-item-has-children">
									<a class="scroll" style="font-size:15px;" href="#page">Guidelines Act & Rule</a>
									<ul style="left:0;">
										<li><a class="scroll" target="_blank" href="The Street Vendors  Act 2014.pdf">Street vendor Act 2014</a></li>
										<li><a class="scroll" target="_blank" href="delhi_scheme_2019.pdf">Delhi Scheme 2019</a></li>
									</ul>
								</li>
								<li class="menu-item">
									<a class="scroll" style="font-size:15px;" href="#gallery">Gallery</a>
								</li>
								<li class="menu-item">
									<a class="scroll" style="font-size:15px;" href="#faq">FAQ's</a>
								</li>
								<li class="menu-item">
									<a class="scroll" style="font-size:15px;" href="#download-app">Download App</a>
								</li>
								<li class="menu-item">
									<a class="scroll" style="font-size:15px;" href="#contact">Contact Us</a>
								</li>
 								<li class="menu-item">
									<a class="scroll" style="font-size:15px;" href="https://dashboard.delhistreethawker.com/login">Login</a>
								</li>
								<li class="menu-item">
									<a class="scroll" style="font-size:15px;" href="hi/">हिन्दी</a>
								</li>
							</ul>
						</nav>
					</div>
					
					<nav id="mobile-menu-01" class="mobile-menu collapse">
						<ul id="mobile-primary-menu" class="menu">
							<li class="menu-item active">
								<a class="scroll" href="#page">Home</a>
							</li>
						    <li class="menu-item-has-children">
								<a class="scroll" href="#page">Guidelines Act & Rule</a>
								<ul style="left:0;">
									<li><a class="scroll" target="_blank" href="The Street Vendors  Act 2014.pdf">Street vendor Act 2014</a></li>
									<li><a class="scroll" target="_blank" href="delhi_scheme_2019.pdf">Delhi Scheme 2019</a></li>
								</ul>
							</li>
							<li class="menu-item">
								<a class="scroll" href="#gallery">Gallery</a>
							</li>
							<li class="menu-item">
								<a class="scroll" href="#faq">FAQ's</a>
							</li>
							<li class="menu-item">
								<a class="scroll" href="#download-app">Download App</a>
							</li>

							<li class="menu-item">
								<a class="scroll" href="#contact">Contact Us</a>
							</li>
							<li class="menu-item">
								<a class="scroll" href="https://dashboard.delhistreethawker.com/login">Login</a>
							</li>
							<li class="menu-item">
								<a class="scroll" href="hi/">हिन्दी</a>
							</li>
						</ul>
					</nav>
				</div>
			</header>
		</section>
		<!--===========================================Static Header================================-->
	
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" style="opacity: 0.84;">

      <div class="item active">
        <img src="images\sliders\static\01.jpg" alt="Zone / Ward" style="width:100%;">
      </div>

      <div class="item">
        <img src="images\sliders\static\02.jpg" alt="Zone / Ward" style="width:100%;">
      </div>
      
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>


	<section id="about-us" class="os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0s">    
		<div class="container">
			<div align="center">
				<div class="section-title" style="padding-top: 20px;">
					<h4>Application Status</h4>
					<div class="heading-bottom">
						<div class="line"></div>
						<div class="box-small"></div>
						<div class="big"></div>
						<div class="box-small"></div>
						<div class="line"></div>
					</div>
				</div>
				<form id="uri-form" action="survey-status" method="POST" style="margin-top: 38px;">
					<input style="width: 320px; height: 60px;" class="form-control" type="text" placeholder="Enter URI / Adhar Number" name="uri_id" id="uri_id" required="">
					<button type="button"  class="btn btn-form" onclick="check_app_status()">Check</button>
				</form>
			</div>
			<div class="row about-block-main clearfix"><!--about us features block -->
				<div class="col-xs-12 col-sm-3 col-md-3"><!--about us feature 1st -->
					<div class="about-block clearfix">
						<a target="_blank" href="Street Hawker Survey Dates.pdf">
							<div class='circle'>
								<i class="fa fa-calendar" aria-hidden="true"></i>
							</div>
							<div class="heading">
								<h6>Survey Dates</h6>
							</div>
						</a>
					</div>
				</div><!--about us feature 1st closed -->

				<div class="col-xs-12 col-sm-3 col-md-3"><!--about us feature 2nd -->
					<div class="about-block clearfix">
						<a target="_blank" href="essential-doc.pdf">
							<div class='circle'>
								<i class="fa fa-file" aria-hidden="true"></i>
							</div>
							<div class="heading">
								<h6>Essential Documents</h6>
							</div>
						</a>
					</div>
				</div><!--about us feature 2nd closed -->

				<div class="col-xs-12 col-sm-3 col-md-3"><!--about us feature 3rd -->
					<div class="about-block clearfix">
						<a class="scroll" href="#report">
							<div class='circle'>
								<i class="fa fa-comments" aria-hidden="true"></i>
							</div>
							<div class="heading">
								<h6>Grievances & Complaints</h6>
							</div>
						</a>
					</div>
				</div><!--about us feature 3rd closed -->

				<div class="col-xs-12 col-sm-3 col-md-3"><!--about us feature 4th -->
					<div class="about-block clearfix">
						<a target="_blank" href="https://dashboard.delhistreethawker.com/print-survey-form">
							<div class='circle'>
								<i class="fa fa-file-text" aria-hidden="true"></i>
							</div>
							<div class="heading">
								<h6>Download Application Form</h6>
							</div>
						</a>
					</div>
				</div><!--about us feature 4th closed -->

			</div><!--about us features block closed -->
		</div><!--about container closed closed -->
	</section>
	
		<section id="about-us-intro" class="os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0s">    
			<div class="container"> 
				<div class="row about-intro-main clearfix">
					<div class="col-xs-12">
						<div class="">
							<div class="section-title">
								<h4>Urban Livelihood</h4>
								<div class="heading-bottom">
									<div class="line"></div>
									<div class="box-small"></div>
									<div class="big"></div>
									<div class="box-small"></div>
									<div class="line"></div>
								</div>
							</div>
								<p style="text-align: justify; font-size: 17px;">It is estimated that about 4 to 5 Lakh street vendors and hawkers are operating in Delhi today and are an integral part of the functioning of Delhi. In order to recognize the street vendors and formalize their operations, Govt. of India has enacted the Street Vendors (Protection of Livelihood and Regulation of Street Vending) Act, 2014 and subsequently, Govt. of NCT of Delhi has notified the Delhi Street Vendors (Protection of Livelihood and Regulation of Street Vending) Rules, 2017 and Delhi Street Vendors (Protection of Livelihood and Regulation of Street Vending) Scheme, 2019. UD DEPARTMENT surveying to collect data of Vendors/ Hawkers/Weekly Markets in different Zones and create an MIS database for future interventions for regularization of street vending in Delhi.</p>
						</div>
					</div>
				</div>
					
			</div><!--about container closed closed -->
		</section>

		
					
		<!--=============================LATEST CAUSES SECTION ==================================-->
		<section id="about-us-intro" class="os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0s">    
			<div class="container"> 
				<div class="section-title"><!--section header-->
					<h4>Survey Overview</h4>
					<div class="heading-bottom">
						<div class="line"></div>
						<div class="box-small"></div>
						<div class="big"></div>
						<div class="box-small"></div>
						<div class="line"></div>
					</div>
				</div><!--section header closed-->
				<div class="row about-intro-main clearfix">
					<div class="col-xs-12 col-sm-6 col-md-6 margin-bottom-30">
						<div id="survey_map" style="height: 400px;"></div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 margin-bottom-30">
						<div class="about-intro-text">
							<h5>Delhi Corporations</h5>
							<br>
							<div class="row">
							    <div class="col-md-3">
							      <div class="card-counter" style="background:#15285c;">
						      		<h4 style="color:white;"><?php echo $survey_stats["total"];?></h4>
						        	<span class="count-name" style="color:white;">Total</span>
							      </div>
							    </div>

							    <div class="col-md-3">
							      <div class="card-counter" style="background:#15285c;">
							      	<h4 style="color:white;"><?php echo $survey_stats["approved"];?></h4>
							        <span class="count-name" style="color:white;">Approved</span>
							      </div>
							    </div>
							</div>
							<br>
							<ul class="list-group list-group-flush">
							  <li class="list-group-item">East Delhi Municipal Corporation</li>
							  <li class="list-group-item">South Delhi Municipal Corporation</li>
							  <li class="list-group-item">North Delhi Municipal Corporation</li>
							  <li class="list-group-item">New Delhi Municipal Corporation</li>
							  <li class="list-group-item">Delhi Cantonment Board</li>
							</ul>
						</div>
					</div>
				</div>
					
			</div><!--about container closed closed -->
		</section>
		<!--=============================FUNFACTS section==================================-->
			
			
		<!--=============================GALLARY SECTION ==================================-->
	
		<section id="gallery" class="os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0s"><!-- gallary section -->
			<div class="container-fluid">
				<div class="section-title"><!--section header-->
					<h4>Gallery</h4>
					<div class="heading-bottom">
						<div class="line"></div>
						<div class="box-small"></div>
						<div class="big"></div>
						<div class="box-small"></div>
						<div class="line"></div>
					</div>
				</div><!--section header closed-->

			</div><!-- row-fluid end-->
				<div class="row" style="padding: 40px;">
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/shahadra_south_Img1.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>View</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/shahadra_south_Img1.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Shahadara-South_Img3.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>View</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Shahadara-South_Img3.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Shahadara-South_Img2.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>View</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Shahadara-South_Img2.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Shahadara-North_Img4.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>View</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Shahadara-North_Img4.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Shahadara-North_Img2.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>View</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Shahadara-North_Img2.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Shahadara-North_Img3.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>View</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Shahadara-North_Img3.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>
		    				    				    				    				    				    				    				    				    
				</div>
				<div align="center"><br><a href="gallery" class="btn-form btn">View More</a></div>
		</section><!-- gallary section end-->
	
		<!--============================= FAQ Section ==================================-->
		<section id="faq" class="os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="padding: 25px 25px 100px 25px;">
	 		<div class="container-fluid">
				<div class="section-title"><!--section header-->
					<h4>FAQ</h4>
					<div class="heading-bottom">
						<div class="line"></div>
						<div class="box-small"></div>
						<div class="big"></div>
						<div class="box-small"></div>
						<div class="line"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6" style="padding: 0px 25px 0px 25px;">
                        <div class="pxlr-club-faq">
                            <div class="content">
                                <div class="panel-group" id="accordion" role="tablist"
                                     aria-multiselectable="true">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" id="headingOne" role="tab">
                                            <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse"
                                                   data-parent="#accordion" href="#collapseOne"
                                                   aria-expanded="false" aria-controls="collapseOne">01.
                                                    Question 1 ? <i
                                                            class="pull-right fa fa-plus"></i></a>
                                            </h4>
                                        </div>
                                        <div class="panel-collapse collapse" id="collapseOne" role="tabpanel"
                                             aria-labelledby="headingOne">
                                            <div class="panel-body pxlr-faq-body">
                                                <p>Answer.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" id="headingTwo" role="tab">
                                            <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse"
                                                   data-parent="#accordion" href="#collapseTwo"
                                                   aria-expanded="false" aria-controls="collapseTwo">02. Question 2 ? 
                                                   <i class="pull-right fa fa-plus"></i></a>
                                            </h4>
                                        </div>
                                        <div class="panel-collapse collapse" id="collapseTwo" role="tabpanel"
                                             aria-labelledby="headingTwo">
                                            <div class="panel-body pxlr-faq-body">
                                                <p>Answer.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" id="headingThree" role="tab">
                                            <h4 class="panel-title"><a class="collapsed" role="button"
                                                                       data-toggle="collapse"
                                                                       data-parent="#accordion"
                                                                       href="#collapseThree"
                                                                       aria-expanded="false"
                                                                       aria-controls="collapseThree">03. Question 3 ? 
                                            <i class="pull-right fa fa-plus"></i></a></h4>
                                        </div>
                                        <div class="panel-collapse collapse" id="collapseThree" role="tabpanel"
                                             aria-labelledby="headingThree">
                                            <div class="panel-body pxlr-faq-body">
                                                <p>Answer.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        			<div class="col-md-6" style="padding: 0px 25px 0px 25px;">
	                    <div class="pxlr-club-faq">
	                        <div class="content">
	                            <div class="panel-group" id="accordion" role="tablist"
	                                 aria-multiselectable="true">
	                                <div class="panel panel-default">
	                                    <div class="panel-heading" id="headingFour" role="tab">
	                                        <h4 class="panel-title">
	                                            <a class="collapsed" role="button" data-toggle="collapse"
	                                               data-parent="#accordion" href="#collapseFour"
	                                               aria-expanded="false" aria-controls="collapseFour">04.
	                                                Question 4 ?  <i
	                                                        class="pull-right fa fa-plus"></i></a>
	                                        </h4>
	                                    </div>
	                                    <div class="panel-collapse collapse" id="collapseFour" role="tabpanel"
	                                         aria-labelledby="headingFour">
	                                        <div class="panel-body pxlr-faq-body">
	                                            <p>Answer.</p>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="panel panel-default">
	                                    <div class="panel-heading" id="headingFive" role="tab">
	                                        <h4 class="panel-title">
	                                            <a class="collapsed" role="button" data-toggle="collapse"
	                                               data-parent="#accordion" href="#collapseFive"
	                                               aria-expanded="false" aria-controls="collapseFive">05. 
	                                               Question 5 ?  
	                                               <i class="pull-right fa fa-plus"></i></a>
	                                        </h4>
	                                    </div>
	                                    <div class="panel-collapse collapse" id="collapseFive" role="tabpanel"
	                                         aria-labelledby="headingFive">
	                                        <div class="panel-body pxlr-faq-body">
	                                            <p>Answer.</p>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="panel panel-default">
	                                    <div class="panel-heading" id="headingSix" role="tab">
	                                        <h4 class="panel-title"><a class="collapsed" role="button"
	                                                                   data-toggle="collapse"
	                                                                   data-parent="#accordion"
	                                                                   href="#collapseSix" aria-expanded="false"
	                                                                   aria-controls="collapseSix">06. Question 6 ? 
	                                        <i class="pull-right fa fa-plus"></i></a></h4>
	                                    </div>
	                                    <div class="panel-collapse collapse" id="collapseSix" role="tabpanel"
	                                         aria-labelledby="headingSix">
	                                        <div class="panel-body pxlr-faq-body">
	                                            <p>Answer.</p>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
                    </div>
				</div>
			</div>
		</section>


        <!--=============================NEWS  SECTION ==================================-->
		
		<!--<section  id="testimonial" class="os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0s">
			<article>
				<div class="container">
					<div class="span12">
						<div class="section-title"><!--section header->
							<h5>News & Updates</h5>
							<div class="heading-bottom">
								<div class="line"></div>
								<div class="box-small"></div>
								<div class="big"></div>
								<div class="box-small"></div>
								<div class="line"></div>
							</div>
						</div><!--section header closed->
						<div  id="owl-demo-testimonial" class="testimonials-ct">
							
							<div class="item">
								<div class="testi-content">Survey Started in Keshav Puram 
								</div>
								<div class="testi-cap">- August 30 2020</div>
							</div>
							
							<div class="item">
								<div class="testi-content">Survey Started in Mayur Vihar. 
								</div>
								<div class="testi-cap">- August 30 2020</div>
							</div>
							
							<div class="item">
								<div class="testi-content">Survey Started in Karol Bagh Zone.
								</div>
								<div class="testi-cap">- August 30 2020</div>
							</div>
							
							<div class="item">
								<div class="testi-content">Survey Started in Keshav Puram
								</div>
								<div class="testi-cap">- August 30 2020</div>
							</div>
						</div>
					</div>
				</div>
			</article>
		</section>-->

		<section id="download-app" class="os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="background: #15285c; padding: 18px 25px 15px 25px;">
			<div class="container clearfix">
				<div class="row">
					<div class="col-md-6">
						<h4 class="pull-right" style="color: white; padding-top: 25px;">Download Android Application</h4>
					</div>
					<div class="col-md-6">
						<a class="pull-right" target="_blank" href="https://www.google.com/url?q=https://play.google.com/store/apps/details?id%3Dcom.streethawkerssurveyapp&sa=D&source=hangouts&ust=1598468946858000&usg=AFQjCNFFCQhZn0pu1lLWCc-abUI7_3ibMQ"><img src="images/play_store.png" style="width: 50%;"></a>
					</div>
				</div>
			</div>
		</section>
		
		<!--=============================Complaints==================================-->    
		<section id="report" class="os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="margin: 70px 0px 10px 0px;">
			<div class="container">
				<div class="section-title">
					<h4 style="text-align: center;">Grievances & Complaints</h4>
					<div class="heading-bottom">
						<div class="line"></div>
						<div class="box-small"></div>
						<div class="big"></div>
						<div class="box-small"></div>
						<div class="line"></div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form_main">
		                <h4 class="heading"><strong>Report  </strong> Complaint <span></span></h4>
		                <form id="complaintForm" name="complaintForm">
		                	<div class="form-group">
		                		<select name="zone_id" class="form-control validate_" id="zone_id" class="txt">
									<option value="">-- Select Zone --</option>
									<option value="1">Shahdara South Zone</option>
									<option value="2">Shahdara North Zone</option>
									<option value="3">Central Zone</option>
									<option value="4">South Zone</option>
									<option value="5">West Zone</option>
									<option value="6">Naladgarh Zone</option>
									<option value="7">City-SP Zone</option>
									<option value="8">Karol Bagh Zone</option>
									<option value="9">Keshav Puram</option>
									<option value="10">Narela</option>
									<option value="11">Rohini</option>
									<option value="12">Civil Lines</option>
									<option value="13">No Zone - NDMC</option>
									<option value="14">No Zone - DCB</option>
		                		</select>
		                		<span class="has-error"></span>
		                	</div>
		                	
		                	<div class="form-group">
		                		<input type="text" required="" class="form-control validate_" placeholder="URI Number" name="f_uri_id" id="f_uri_id" class="txt">
		                		<span class="has-error" id="uri_error"></span>
		                    </div>

		                    <div class="form-group">
		                    	<input type="text" required="" class="form-control validate_" placeholder="Full Name" id="name"  name="name" class="txt">
		                    	<span class="has-error"></span>
		                    </div>
		                    
		                    <div class="form-group">
		                    	<input type="text" required="" class="form-control validate_" placeholder="Mobile No" id="phone" name="phone" class="txt">
		                    	<span class="has-error" id="mobile_error"></span>
		                    </div>                   
		                	
		                	<div class="form-group">
		                		 <textarea  placeholder="Your Message" class="form-control validate_" name="msg" id="msg" type="text" class="txt_3"></textarea>
		                		 <span class="has-error"></span>
		                     </div>
		                     <button type="button" id="btn-complaint" class="btn btn-form" onclick="send_form()">Send</button>
		                </form>
            		</div>
            	</div>
				<div class="form_main">
					<div class="col-md-6">
						<form action="view-complaint" method="POST" id="comp-form">
							<h4 class="heading"><strong>Complaint </strong> Status <span></span></h4>
							<div class="form-group">
								<input class="form-control" type="text" name="ref_id" id="ref_id" placeholder="Enter Reference No." class="txt" />
								<button type="button"  class="btn btn-form" onclick="check_complaint()">Check</button>
							 </div>
						</form>
					</div>
				</div>

			</div>
		</section>

		<section id="contact" class="os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0s" style="margin: 70px 0px; 10px; 0px;">
			<div class="container">
				<div class="section-title">
					<h4 style="text-align: center;">Contact Us</h4>
					<div class="heading-bottom">
						<div class="line"></div>
						<div class="box-small"></div>
						<div class="big"></div>
						<div class="box-small"></div>
						<div class="line"></div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form_main">
		                <h4 class="heading"><strong>Need  </strong> Help? <span></span></h4>
		                <div class="form">
			                <form id="contactForm" name="contactForm">
				                    <div class="form-group">
				                    	<input type="text" required="" class="form-control validateContact_" placeholder="Full Name" id="uname"  name="uname" class="txt">
				                    	<span class="has-error"></span>
				                    </div>
				                    
				                    <div class="form-group">
				                    	<input type="text" required="" class="form-control validateContact_" placeholder="Mobile No" id="uphone" name="uphone" class="txt">
				                    	<span class="has-error" id="con-mobile_error"></span>
				                    </div>    

				                 	<div class="form-group">
				                    	<input type="text" class="form-control validateContact_" placeholder="Email" id="uemail" name="uemail" class="txt">
				                    	<span class="has-error" id="con-email_error"></span>
				                    </div>                   
				                	
				                	<div class="form-group">
				                		 <textarea  placeholder="Your Message" class="form-control validateContact_" name="umsg" id="umsg" type="text" class="txt_3"></textarea>
				                		 <span class="has-error"></span>
				                     </div>
				                     <button type="button" id="btn-contact" class="btn btn-form" onclick="send_email()">Send</button>
			                </form>
		            	</div>
            		</div>
            	</div>
				<div class="form_main">
					<div class="col-md-6">
						<h4 class="heading"><strong> Officer </strong> Contact List <span></span></h4>
						<p> Name : Contact </p>
						<p> Name : Contact </p>
						<p> Name : Contact </p>
						<p> Name : Contact </p>
					</div>
				</div>
          
			</div>
		</section>

<?php include('footer.php'); ?>

	</div>
	<!--#page end-->   	
		  <script type="text/javascript">
		    
		   
		      $('.autoscroller').slick({
			      infinite: true,
			      fade: true,
			      cssEase: 'linear',
			      adaptiveHeight: true,
			      autoplay: true,
			      autoplaySpeed: 5000,
		      });
		   
		  </script>

		<script type="text/javascript">
		
			function appjar_validator(type) {
	            var validation_status = 1;
	            $('.'+type).each(function () {
	                var id = $(this).attr('id');
	                if($('#'+id).val() == '' || $('#'+id).val() == null){
	                    $('#'+id).next().text('This field is required.');
	                    $('#'+id).addClass('is-invalid');
	                    validation_status = 0;
	                }else{
	                    $('#'+id).next().text('');
	                    $('#'+id).removeClass('is-invalid');
	                }
	            });

	            if(validation_status == 1){
	                return true
	            }
            }

			function send_form(){

				if(appjar_validator('validate_')){
					
					var zone_id	= $('#zone_id').val();
					var name = $('#name').val();
					var f_uri_id = $('#f_uri_id').val();
					var phone	= $('#phone').val();
					var msg	= $('#msg').val();

					 $.ajax({
		                type: 'POST',
		                url: 'send-message.php',
		                dataType: "json",
		                data: {
		                    name: name,
		                    zone_id:zone_id,
		                    f_uri_id:f_uri_id,
		                    phone:phone,
		                    msg:msg,
		                    _method: 'POST'
		                },
		                success: function (data) {
		                       	if (data.code=='2') {
									swal(data.msg);
									document.getElementById("btn-complaint").disabled = false;
		                    	}
		                    	if(data.code=='200'){
		                    		swal(data.msg);
		                    		document.getElementById("btn-complaint").disabled = false;
									document.getElementById("complaintForm").reset();
		                    	}
		                    	if(data.code=='400'){
		                    		document.getElementById("btn-complaint").disabled = false;
		                    		swal(data.msg);
		                    	}

		                }, error: function (jqXHR, textStatus, errorThrown) {
		                		console.log(errorThrown);
		                }
		            });
				}
			
			}

			function check_complaint(){
				var ref_id = $('#ref_id').val();
				if (ref_id=='') {
					swal('Please enter Valid Reference Number','Reference number can not be empty!','error');
				}else{
						document.getElementById("comp-form").submit();
				}
			}

			function check_app_status(){
				var ref_id = $('#uri_id').val();
				if (ref_id=='') {
					swal('Please enter Valid URI / Adhar Number','This field can not be empty!','error');
				}else{
						document.getElementById("uri-form").submit();
				}
			}

			function send_email(){

				if(appjar_validator('validateContact_')){
					
					var uname = $('#uname').val();
					var uphone	= $('#uphone').val();
					var umsg	= $('#umsg').val();
					var uemail	= $('#uemail').val();
					document.getElementById("btn-contact").disabled = true;
					 $.ajax({
		                type: 'POST',
		                url: 'send-email.php',
		                dataType: "json",
		                data: {
		                    uname: uname,
		                    uphone:uphone,
		                    uemail:uemail,
		                    umsg:umsg,
		                    _method: 'POST'
		                },
		                success: function (data) {
		                	console.log(data);
		                       	if (data.code=='2') {
									swal(data.msg);
									document.getElementById("btn-contact").disabled = false;
		                    	}
		                    	if(data.code=='200'){
		                    		swal('', data.msg, 'success');
									document.getElementById("btn-contact").disabled = false;
									document.getElementById("contactForm").reset();
		                    	}
		                    	if(data.code=='400'){
		                    		swal(data.msg);
									document.getElementById("btn-contact").disabled = false;
		                    	}

		                }, error: function (jqXHR, textStatus, errorThrown) {
		                		console.log(errorThrown);
		                }
		            });
				}
			
			}

	

			$.ajax({
                type: 'GET',
                url: 'map_data.php',
                success: function (data) {
                	var json_data = JSON.parse(data);
                		var response = json_data.response;
                                var survey_loc = [];
                                for(var i=0;i<response.length;i++){

                                    if(response[i].latitude != null && response[i].latitude != '0.0'){
                                        survey_loc[i] = {
                                            'loc' : [response[i].latitude,response[i].longitude],
                                            'title' : response[i].area_name
                                        }
                                    }

                                }


                                var data = survey_loc;

                                // init leaflet map
                                 var leaflet = new L.Map('survey_map', {zoom: 11, center: new L.latLng(28.6113208,77.1517269) });


                                leaflet.addLayer(new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'));

                                // add scale layer
                                L.control.scale().addTo(leaflet);

                                // set custom SVG icon marker
                                var leafletIcon = L.divIcon({
                                    html: `<span class="svg-icon svg-icon-danger svg-icon-3x"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="24" width="24" height="0"/><path d="M5,10.5 C5,6 8,3 12.5,3 C17,3 20,6.75 20,10.5 C20,12.8325623 17.8236613,16.03566 13.470984,20.1092932 C12.9154018,20.6292577 12.0585054,20.6508331 11.4774555,20.1594925 C7.15915182,16.5078313 5,13.2880005 5,10.5 Z M12.5,12 C13.8807119,12 15,10.8807119 15,9.5 C15,8.11928813 13.8807119,7 12.5,7 C11.1192881,7 10,8.11928813 10,9.5 C10,10.8807119 11.1192881,12 12.5,12 Z" fill="#000000" fill-rule="nonzero"/></g></svg></span>`,
                                    bgPos: [10, 10],
                                    iconAnchor: [20, 37],
                                    popupAnchor: [0, -37],
                                    className: 'leaflet-marker'
                                });

                                // set markers
                                data.forEach(function(item){
                                    var marker = L.marker(item.loc, { icon: leafletIcon }).addTo(leaflet);
                                    marker.bindPopup(item.title, { closeButton: false });
                                })

                }, error: function (jqXHR, textStatus, errorThrown) {
						console.log(errorThrown);
                }
            });

		</script>
    </body>
</html>
