<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="user-scalable = yes" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
        
	<title>Delhi Hawkers Survey</title>
	
    <meta name="discription" content="Delhi hawkers survey" >
	<meta name="keyword" content="Delhi hawkers survey">
    <!--================================BOOTSTRAP STYLE SHEETS================================-->
        
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	
    <!--================================ Main STYLE SHEETs====================================-->
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
	
	<!--================================ colors STYLE SHEETs====================================-->
	<link class="alt" href="css/colors/green.css" media="screen" rel="stylesheet"  title="color1" type="text/css">
	
	<!--================================COLORBOX==========================================-->

	<link rel="stylesheet" href="assets/colorbox/colorbox.css" />

	<!--================================FONTAWESOME==========================================-->
		
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        
	<!--================================GOOGLE FONTS=========================================-->
		
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,400,300,300italic,400italic,500,500italic,700,900' rel='stylesheet' type='text/css'>   
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,400italic' rel='stylesheet' type='text/css'>
		
</head>

<body>

    <!--===========================================MAIN NAV======================================-->
	<div id="page">
		<section id="nav">
			<header id="header" class="navbar-static-top">
				<div class="main-header">
					<div class="mobile-menu-wrap"><a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle"></a></div>
					<div class="container">
						<h1 class="logo navbar-brand">
							<a href="./">
								<img src="images/delhi_gov_logo.png">
							</a>
						</h1>

						<nav id="main-menu" role="navigation">
							<ul class="menu">
								<li class="menu-item">
									<a class="scroll" href="./">Home</a>
								</li>
    						    <li class="menu-item-has-children">
    								<a class="scroll" href="#page">Guidelines Act & Rule</a>
    								<ul style="left:0;">
    									<li><a class="scroll" target="_blank" href="The Street Vendors  Act 2014.pdf">Street vendor Act 2014</a></li>
    									<li><a class="scroll" target="_blank" href="delhi_scheme_2019.pdf">Delhi Scheme 2019</a></li>
    								</ul>
    							</li>
								<li class="menu-item">
									<a class="scroll" href="./#gallary">Gallery</a>
								</li>
								<li class="menu-item">
									<a class="scroll" href="./#faq">FAQ's</a>
								</li>
								<li class="menu-item">
									<a class="scroll" href="./#download-app">Download App</a>
								</li>
								<li class="menu-item">
									<a class="scroll" href="#contact">Contact Us</a>
								</li>
 								<li class="menu-item">
									<a class="scroll" href="https://dashboard.delhistreethawker.com">Login</a>
								</li>
								<li class="menu-item">
									<a class="scroll" href="hi/">हिन्दी</a>
								</li>
							</ul>
						</nav>
					</div>
					
					<nav id="mobile-menu-01" class="mobile-menu collapse">
						<ul id="mobile-primary-menu" class="menu">
							<li class="menu-item">
								<a class="scroll" href="./">Home</a>
							</li>
						    <li class="menu-item-has-children">
								<a class="scroll" href="#page">Guidelines Act & Rule</a>
								<ul style="left:0;">
									<li><a class="scroll" target="_blank" href="The Street Vendors  Act 2014.pdf">Street vendor Act 2014</a></li>
									<li><a class="scroll" target="_blank" href="delhi_scheme_2019.pdf">Delhi Scheme 2019</a></li>
								</ul>
							</li>
							<li class="menu-item">
								<a class="scroll" href="./#gallary">Gallery</a>
							</li>
							<li class="menu-item">
								<a class="scroll" href="./#faq">FAQ's</a>
							</li>
							<li class="menu-item">
								<a class="scroll" href="./#download-app">Download App</a>
							</li>

							<li class="menu-item">
								<a class="scroll" href="#contact">Contact Us</a>
							</li>
							<li class="menu-item">
								<a class="scroll" href="https://dashboard.delhistreethawker.com">Login</a>
							</li>
							<li class="menu-item">
								<a class="scroll" href="hi/">हिन्दी</a>
							</li>
						</ul>
					</nav>
				</div>
			</header>
		</section>
		<!--===========================================Static Header================================-->

			
		<!--=============================GALLARY SECTION ==================================-->
	
		<section style="margin-top: 140px;" id="gallary" class="os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0s"><!-- gallary section -->
			<div class="container-fluid">
				<div class="section-title"><!--section header-->
					<h4>Gallery</h4>
					<div class="heading-bottom">
						<div class="line"></div>
						<div class="box-small"></div>
						<div class="big"></div>
						<div class="box-small"></div>
						<div class="line"></div>
					</div>
				</div><!--section header closed-->

				<div class="row" style="padding: 40px;">
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/shahadra_south_Img1.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/shahadra_south_Img1.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Shahadara-South_Img3.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Shahadara-South_Img3.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Shahadara-South_Img2.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Shahadara-South_Img2.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Shahadara-North_Img4.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Shahadara-North_Img4.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Shahadara-North_Img2.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Shahadara-North_Img2.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Shahadara-North_Img3.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Shahadara-North_Img3.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Shahadara-North_Img1.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Shahadara-North_Img1.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Sadar-City_Img3.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Sadar-City_Img3.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>	
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Sadar-City_Img2.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Sadar-City_Img2.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Sadar-City_Img1.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Sadar-City_Img1.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>	
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Sadar-City Zone Mtg_Img4.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Sadar-City Zone Mtg_Img4.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>	
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Rohini_Img4.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Rohini_Img4.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>	
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Rohini_Img3.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Rohini_Img3.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>	
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Rohini_Img2.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Rohini_Img2.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>	
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Rohini_Img1.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Rohini_Img1.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>	
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Rajouri-Garden_Img2.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Rajouri-Garden_Img2.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>	
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Rajouri-Garden_Img3.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Rajouri-Garden_Img3.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>	
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Rajouri-Garden_Img1.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Rajouri-Garden_Img1.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>	
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Pilot-Survey_Img6.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Pilot-Survey_Img6.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>	
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Pilot-Survey_Img5.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Pilot-Survey_Img5.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>	
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Pilot-Survey_Img4.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Pilot-Survey_Img4.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>	
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Pilot-Survey_Img3.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Pilot-Survey_Img3.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>					    				    				    				    				    				    				    				    				    				    				    				    
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Pilot-Survey_Img2.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Pilot-Survey_Img2.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>	
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Pilot-Survey_Img1.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Pilot-Survey_Img1.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>	
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Karol-Bagh_Img1.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Karol-Bagh_Img1.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>	
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/IMG_20200824_204203.jpg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/IMG_20200824_204203.jpg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>	

				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/IMG_20200824_204136.jpg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/IMG_20200824_204136.jpg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>	

				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Green-Park_Img5.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Green-Park_Img5.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>	
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Green-Park_Img4.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Green-Park_Img4.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>	
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Green-Park_Img3.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Green-Park_Img3.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>	
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Green-Park_Img2.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Green-Park_Img2.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>	
				    <div class="col-md-4" style="padding-bottom: 15px;">
				        <div class="gallary-content">
									<img src="images/gallary/thumb/Green-Park_Img1.jpeg" alt="gallary"/>
									<div class="gallary-overlay">
										<div class="gallary-title">
											<h5>VIEW</h5>
										</div>
										<div class="gallary-links">
											<a class="group1" href="images/gallary/Green-Park_Img1.jpeg"><i class="fa fa-search"></i></a>
										</div>
									</div>
								</div>
				    </div>					    				    				    				    				    				    				    				    				    

				</div>

				    <br><br>
				    
			</div><!-- row-fluid end-->
		</section><!-- gallary section end-->

		<!--=============================CALL-OUT SECTION ==================================-->

		<!--================================FOOTER===========================================-->
	<!--================================FOOTER===========================================-->
<?php include('footer.php'); ?>
	</div>


    </body>
</html>
